<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'private-esaunggul-database-do-user-4550717-0.a.db.ondigitalocean.com';
$CFG->dbname    = 'moodle38';
$CFG->dbuser    = 'doadmin';
$CFG->dbpass    = 'c8ipy9boosi3srbf';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 25060,
  'dbsocket' => '',
);

$CFG->wwwroot   = 'https://moodle-do.esaunggul.ac.id';
$CFG->dataroot  = '/moodledata';
$CFG->admin     = 'admin';
$CFG->slasharguments = false;
$CFG->directorypermissions = 0777;

// Enable when setting up advanced reverse proxy load balancing configurations,
// it may be also necessary to enable this when using port forwarding.
#$CFG->reverseproxy = true;
//
// Enable when using external SSL appliance for performance reasons.
// Please note that site may be accessible via http: or https:, but not both!
//
$CFG->sslproxy = true;

/*
$CFG->session_redis_host = '172.31.114.10';
////$CFG->session_redis_host ='172.21.0.151';
$CFG->session_redis_port = 6379;  // Optional.
$CFG->session_redis_database = 'elearning-staging';  // Optional, default is db 0.
$CFG->session_redis_prefix = 'elearning-staging'; // Optional, default is don't set one.
$CFG->session_redis_acquire_lock_timeout = 120;
$CFG->session_redis_lock_expire = 7200;
*/


//$CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
//$CFG->debugdisplay = 1;             // NOT FOR PRODUCTION SERVERS!

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
