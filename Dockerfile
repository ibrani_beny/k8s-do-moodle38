FROM debian
  
# Install.
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN \
  apt-get update && \
  apt-get -y upgrade && \
  DEBIAN_FRONTEND=noninteractive apt-get install -q -y postfix && \
  apt-get install -y php-mongodb php-redis nginx php php-pgsql  php-gd php-curl php-fpm php-cgi php-cli php-zip  && \
  apt-get install -y supervisor  postgresql-client postgresql-client-common postgresql-contrib && \
  apt-get install -y php-memcached php-soap php-ctype  php-zip php-simplexml  php-dom php-xml php-json php-intl php-fpm php-common php-mbstring php-xmlrpc php-soap php-gd php-xml php-intl php-mysql php-cli php-ldap php-zip php-curl libpq-dev  rsyslog  && \
  apt-get install -y net-tools curl memcached git htop man unzip vim wget
RUN apt-get install -y --no-install-recommends --no-install-suggests supervisor cron


COPY cronmoodle /etc/cron.d/cronmoodle
RUN chmod 644 /etc/cron.d/cronmoodle
RUN crontab /etc/cron.d/cronmoodle

COPY . /var/www/html/
RUN chown www-data:www-data /var/www
RUN chown -R www-data:www-data /var/www/*
RUN chmod 755 -R /var/www/*
RUN mkdir /var/www/html/localcache
RUN mkdir /moodledata
RUN chmod 777 /moodledata
RUN chown www-data:www-data /var/www/html/localcache

RUN mkdir -p /run/php
COPY nginx.conf /etc/nginx/nginx.conf
COPY php.ini /etc/php/7.3/fpm/php.ini
COPY default /etc/nginx/sites-enabled/default
CMD chown www-data:www-data /var/www
CMD chown -R www-data:www-data /var/www/*
COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf

CMD ["/usr/bin/supervisord"]
RUN rm -rf /var/www/html/cronmoodle
RUN rm -rf /var/www/html/index.html
RUN rm -rf /var/www/html/Dockerfile
RUN rm -rf /var/www/html/default
RUN rm -rf /var/www/html/php.ini
RUN rm -rf /var/www/html/www.conf
RUN rm -rf /var/www/html/supervisor.conf
RUN rm -rf /var/www/html/.gitlab-ci.yaml
RUN rm -rf /var/www/html/nginx.conf
